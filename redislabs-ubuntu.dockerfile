FROM redislabs/redis:6.0.6-35

USER root:root

COPY ./redis/init_script.sh /tmp/init_script.sh

RUN adduser --disabled-password --gecos "Lab User,,," labuser && \
    usermod -aG redislabs,sudo labuser

RUN apt-get update && \
    apt-get install -y git && \
    apt-get install -y openssh-server
