FROM krishnasrinivas/wetty

USER root

RUN apk --no-cache add shadow && \
    adduser --disabled-password --gecos "Lab User,,," labuser && \
    usermod -aG wheel labuser

RUN echo 'alias n1-status="ssh -t re-n1 /opt/redislabs/bin/rladmin status"' >> ~labuser/.ashrc

CMD node bin
#RUN apt update && \
#    apt install -y git
