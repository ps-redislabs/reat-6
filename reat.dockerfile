FROM ubuntu

ARG GCLOUD_SERVICE_KEY

RUN apt-get update && \
    apt-get install -y docker && \
    apt-get install -y docker-compose && \
    apt-get install -y gnupg2 pass && \
    apt-get install -y python3-pip && \
    python3 -m pip install envsubst


RUN echo "$GCLOUD_SERVICE_KEY" > gcloud-service-key.json && \
     docker login -u _json_key --password-stdin https://us.gcr.io < gcloud-service-key.json && \
     rm gcloud-service-key.json

COPY docker-compose.yml docker-compose.yml
COPY nginx.conf nginx.conf
COPY prometheus prometheus
COPY alertmanager alertmanager
COPY grafana grafana
COPY index.html.template index.html.template
COPY start.sh start.sh
COPY redis redis
COPY wait-for-code.sh wait-for-code.sh
COPY bind bind
COPY cassandra cassandra

CMD bash start.sh > start.log 2>&1
