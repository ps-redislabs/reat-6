from locust import HttpLocust, TaskSet, task, between, web
import locust.events
from random import seed, randint
import time
import atexit
from prometheus_client import CollectorRegistry, Counter, push_to_gateway

registry = CollectorRegistry()
c_success = Counter('success', 'Successful Requests', registry=registry)
c_failures = Counter('failure', 'Failed Requests', registry=registry)

total_records = 250000
current_percent = 1
affected_records = total_records / ( current_percent * 100 )

@web.app.route("/rate/status")
def rate_status():
    return { "current_percent": current_percent, "total_records": total_records, "affected_records": affected_records }

@web.app.route("/rate/<int:percent>")
def rate_percent(percent):
    try:
        if percent >= 1 and percent <= 100:
            global current_percent, affected_records
            current_percent = percent
            affected_records = total_records * ( current_percent / 100 )
            return rate_status()
        else:
            raise ValueError
    except ValueError:
        return { "error": "Please enter a valid number between 1 and 100"}

class SimpleLocustTest(TaskSet):

    @task
    def get_something(self):
        custid = self.get_random_customer()
        self.client.get("/customer/%s" % custid)

    def get_random_customer(self):
        custid = randint(1, affected_records)
        return '{:0>10}'.format(custid)

class LocustTests(HttpLocust):
    seed(1)
    task_set = SimpleLocustTest
    wait_time = between(0, 1)

    def __init__(self):
        super(LocustTests, self).__init__()
        #locust.events.request_success += self.hook_request_success
        #locust.events.request_failure += self.hook_request_fail 
        #atexit.register(self.exit_handler)

    def hook_request_success(self, request_type, name, response_time, response_length):
        #self.sock.send("%s %d %d\n" % ("performance." + name.replace('.', '-'), response_time,  time.time()))
        print("%s %d %d\n" % (name, response_time,  time.time()))
        c_success.inc()
        push_to_gateway('prom-pushgateway:9091', job='batchA', registry=registry)


    def hook_request_fail(self, request_type, name, response_time, response_length, exception):
        #self.request_fail_stats.append([name, request_type, response_time, exception])
        print("%s %s %s %s" % (name, request_type, response_time, exception))
        c_failures.inc()
        push_to_gateway('prom-pushgateway:9091', job='batchA', registry=registry)


    def exit_handler(self):
        print("shutting down...")
        #self.sock.shutdown(socket.SHUT_RDWR)
        #self.sock.close()
