set -e
rm -rf buildenv
virtualenv buildenv
source buildenv/bin/activate
rm -rf builddir
mkdir -p builddir/sl
cd builddir/sl
cp -r ../../code/sl/* .
rm -f *.log*
mkdir deps
python3 -m pip download -d deps -r requirements.txt 
cd ..
BUILD_ID=$(cat /dev/urandom | tr -dc 'a-z0-9' | fold -w 6 | head -n 1)
BUILD_FILE="sl.$(echo $BUILD_ID).tar.gz"
tar cvf $BUILD_FILE sl/
gsutil cp $BUILD_FILE gs://ps-redislabs-kgtkj/
