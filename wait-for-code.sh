#!/bin/bash

TIMEOUT="${TIMEOUT:-600}"
CODE="${CODE:-200}"
seconds=0

echo Waiting up to $TIMEOUT seconds for HTTP $CODE from $URL 
until [ "$seconds" -gt "$TIMEOUT" ]; do
  printf .
  lastcode=$(curl --output /dev/null -k --silent --max-time $TIMEOUT --head -w "%{http_code}" --fail $URL)
  if [ $lastcode -eq $CODE ]; then
     break
  fi
  sleep 5
  seconds=$((seconds+5))
done

if [ "$seconds" -lt "$TIMEOUT" ]; then
  echo OK after $seconds seconds
else
  echo "ERROR: Timed out wating for HTTP 200 from" $URL >&2
  exit 1
fi
