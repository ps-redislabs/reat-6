dockerd &

envsubst < index.html.template > index.html

export RE_USER=admin@rl.org

docker-compose up -d

main_nodes=( re-n1 re-s1 )
follower_nodes=( re-n2 re-n3 re-s2 re-s3 )

all_nodes=( re-n1 re-n2 re-n3 re-s1 re-s2 re-s3 )
for i in "${all_nodes[@]}"
do 
   #wait for admin port
   docker cp wait-for-code.sh $i:/tmp/wait-for-code.sh
   docker exec -e URL=https://$i:9443 -e CODE=503 $i /bin/bash /tmp/wait-for-code.sh 

   #enable port 53
   docker exec --user root --privileged $i /bin/bash /tmp/init_script.sh
done

#create cluster 1
export CLUSTER=re-cluster1.ps-redislabs.org
export IP=172.16.22.21
server="re-n1"
envsubst < redis/create_cluster.json.template > create_cluster.json
docker cp create_cluster.json $server:/tmp/create_cluster.json
docker exec $server curl -k -v --silent --fail -H 'Content-Type: application/json' -d @/tmp/create_cluster.json  https://$server:9443/v1/bootstrap/create_cluster
sleep 2
envsubst < redis/join_cluster.json.template > join_cluster.json
server="re-n2"
docker cp join_cluster.json $server:/tmp/join_cluster.json
docker exec $server curl -k -v --silent --fail -H 'Content-Type: application/json' -d @/tmp/join_cluster.json  https://$server:9443/v1/bootstrap/join_cluster
server="re-n3"
docker cp join_cluster.json $server:/tmp/join_cluster.json
docker exec $server curl -k -v --silent --fail -H 'Content-Type: application/json' -d @/tmp/join_cluster.json  https://$server:9443/v1/bootstrap/join_cluster

export CLUSTER=re-cluster2.ps-redislabs.org
export IP=172.16.22.41
server="re-s1"
envsubst < redis/create_cluster.json.template > create_cluster.json
docker cp create_cluster.json $server:/tmp/create_cluster.json
docker exec $server curl -k -v --silent --fail -H 'Content-Type: application/json' -d @/tmp/create_cluster.json  https://$server:9443/v1/bootstrap/create_cluster
envsubst < redis/join_cluster.json.template > join_cluster.json
sleep 2
server="re-s2"
docker cp join_cluster.json $server:/tmp/join_cluster.json
docker exec $server curl -k -v --silent --fail -H 'Content-Type: application/json' -d @/tmp/join_cluster.json  https://$server:9443/v1/bootstrap/join_cluster
server="re-s3"
docker cp join_cluster.json $server:/tmp/join_cluster.json
docker exec $server curl -k -v --silent --fail -H 'Content-Type: application/json' -d @/tmp/join_cluster.json  https://$server:9443/v1/bootstrap/join_cluster

#set up ssh
docker exec --user labuser bastion sh -c "mkdir ~/.ssh/ && ssh-keygen -t rsa -b 4096 -f ~/.ssh/id_rsa -q -N \"\""
rm -rf .sshtmp/
docker cp bastion:/home/labuser/.ssh/ .sshtmp/
for i in "${all_nodes[@]}"
do 
   docker exec --user labuser $i /bin/bash -c "mkdir -p ~labuser/.ssh"
   docker cp .sshtmp/id_rsa.pub $i:/home/labuser/.ssh/authorized_keys
   docker exec --user root $i /bin/bash -c "chown -R labuser:labuser /home/labuser/.ssh/"
   docker exec --user labuser bastion sh -c "ssh-keyscan -H $i >> ~/.ssh/known_hosts"
done

#install gears dependencies
for i in "${all_nodes[@]}"
do 
   docker cp redis/redisgears-dependencies.linux-bionic-x64.1.0.1.tgz $i:/tmp/redisgears-dependencies.linux-bionic-x64.1.0.1.tgz
   docker cp redis/install_gears_deps.sh $i:/tmp/install_gears_deps.sh
   docker exec --user root --privileged $i /bin/bash /tmp/install_gears_deps.sh
done

#install gears
for i in "${main_nodes[@]}"
do 
   docker cp redis/redisgears.linux-bionic-x64.1.0.1.zip $i:/tmp/redisgears.linux-bionic-x64.1.0.1.zip
   docker exec $i curl -k -v -u $RE_USER:$PASSWORD -F "module=@/tmp/redisgears.linux-bionic-x64.1.0.1.zip" https://$i:9443/v1/modules
done

#install rgsync
for i in "${main_nodes[@]}"
do 
   docker exec --user root --privileged $i apt install -q -y python3-pip
   docker cp redis/rgsync-1.0.1.linux-bionic-x64.zip $i:/tmp/rgsync-1.0.1.linux-bionic-x64.zip
   docker exec --user root $i python3 -m pip install gears-cli
done

#create gears-db
docker cp redis/create_gears_db.json.template re-n1:/tmp/create_gears_db.json
docker exec re-n1 curl -X POST -H 'cache-control: no-cache' -H 'Content-type: application/json' -u $RE_USER:$PASSWORD -d @/tmp/create_gears_db.json -k https://localhost:9443/v1/bdbs
sleep 5 
docker exec -e LC_ALL=C.UTF-8 -e LANG=C.UTF-8 re-n1 gears-cli import-requirements \
    --host re-n1 --port 14092 \
    --requirements-path /tmp/rgsync-1.0.1.linux-bionic-x64.zip
   
#set cassandra config
docker exec re-n1 rladmin tune db db:1 module_name rg module_config_params "cassandra_host cassandra"
docker exec re-n1 rladmin tune db db:1 module_name rg module_config_params "cassandra_pass cassandra"

docker-compose up

wait $!
